use std::fs::File;
use std::io::{BufRead, BufReader};

pub fn read_text_file(filename: &str) -> Result<Vec<String>, std::io::Error> {
	let file = File::open(filename)?;
	BufReader::new(file).lines().collect()
}

#[derive(Debug, Default, Copy, Clone, Eq, PartialEq, Hash)]
pub struct Point {
	pub x: i32,
	pub y: i32,
}

#[derive(Debug)]
pub struct Bounds {
	pub top_l: Point,
	pub bot_r: Point,
}

impl Bounds {
	pub fn iter(&self) -> BoundsPointsIterator {
		BoundsPointsIterator {
			b: &self,
			cur: self.top_l,
		}
	}
}

pub struct BoundsPointsIterator<'a> {
	b: &'a Bounds,
	cur: Point,
}

impl<'a> Iterator for BoundsPointsIterator<'a> {
	type Item = Point;
	fn next(&mut self) -> Option<Self::Item> {
		if self.cur.x > self.b.bot_r.x || self.cur.y > self.b.bot_r.y {
			None
		} else {
			let ret = self.cur;
			if self.cur.x == self.b.bot_r.x {
				self.cur.x = self.b.top_l.x;
				self.cur.y += 1;
			} else {
				self.cur.x += 1;
			}
			Some(ret)
		}
	}
}

// impl<'a> ExactSizeIterator for BoundsPointsIterator<'a> {
// 	fn len(&self) -> usize {
// 		((self.b.top_l.x - self.b.bot_r.x).abs() * (self.b.top_l.y - self.b.bot_r.y).abs()
// 			+ (self.b.top_l.y - self.b.bot_r.y).abs()
// 			+ (self.b.top_l.x - self.b.bot_r.x).abs()
// 			+ 1) as usize
// 	}
// }

// impl<'a> DoubleEndedIterator for BoundsPointsIterator<'a> {
// 	fn next_back(&mut self) -> Option<Self::Item> {
// 		if self.cur.x == self.b.top_l.x && self.cur.y == self.b.top_l.y {
// 			None
// 		} else {
// 			let ret = self.cur;
// 			if self.cur.x == self.b.top_l.x {
// 				self.cur.x = self.b.bot_r.x;
// 				self.cur.y -= 1;
// 			} else {
// 				self.cur.x -= 1;
// 			}
// 			Some(ret)
// 		}
// 	}
// }

pub fn find_search_space_edges(points: &[Point]) -> Bounds {
	//assumes at least one point
	let mut tl = points[0];
	let mut br = points[0];
	for p in &points[1..] {
		if p.x < tl.x {
			tl.x = p.x
		}
		if p.x > br.x {
			br.x = p.x
		}
		if p.y < tl.y {
			tl.y = p.y
		}
		if p.y > br.y {
			br.y = p.y
		}
	}
	Bounds {
		top_l: tl,
		bot_r: br,
	}
}
