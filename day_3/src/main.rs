use util;

struct Board {
	data: Vec<Vec<usize>>,
}

impl Board {
	fn new(width: usize, height: usize) -> Board {
		Board {
			data: vec![vec![0; width]; height],
		}
	}
}

#[derive(Default, Debug)]
struct Claim {
	id: usize,
	h_offset: usize,
	v_offset: usize,
	width: usize,
	height: usize,
}

//will panic!() if it can't parse
fn parse_claim(claim: &str) -> Claim {
	// assuming claim is formed like:
	// #1 @ 1,3: 4x4
	let mut tokens_iter = claim.split(|x: char| !x.is_numeric()).filter(|x| *x != "");
	Claim {
		id: tokens_iter.next().unwrap().parse().unwrap(),
		//inverse horizonal and vertical
		v_offset: tokens_iter.next().unwrap().parse().unwrap(),
		h_offset: tokens_iter.next().unwrap().parse().unwrap(),
		//inverse width and height
		height: tokens_iter.next().unwrap().parse().unwrap(),
		width: tokens_iter.next().unwrap().parse().unwrap(),
	}
}

fn stamp_claims(b: &mut Board, claims: &[Claim]) -> usize {
	let mut num_overlapping = 0_usize;
	for c in claims {
		for i in c.h_offset..c.h_offset + c.width {
			for j in c.v_offset..c.v_offset + c.height {
				if b.data[i][j] == 1 {
					num_overlapping += 1;
				}
				b.data[i][j] += 1;
			}
		}
	}
	num_overlapping
}

fn is_claim_overlapping(b: &Board, c: &Claim) -> bool {
	let mut sum = 0_usize;
	for i in c.h_offset..c.h_offset + c.width {
		for j in c.v_offset..c.v_offset + c.height {
			sum += b.data[i][j];
		}
	}
	sum != (c.width * c.height)
}

fn main() -> Result<(), std::io::Error> {
	//I could append the fabric dynamically
	//since the problem indirectly suggests that all claims
	//are within bounds
	let mut fabric = Board::new(1100, 1100);
	let input: Vec<Claim> = util::read_text_file("input.txt")?
		.iter()
		.map(|x| parse_claim(x))
		.collect();

	//part 1
	let overlapping = stamp_claims(&mut fabric, &input);
	println!("{}", overlapping);

	//part 2
	for i in &input {
		if !is_claim_overlapping(&fabric, i) {
			println!("non overlapping claim found: {}", i.id);
			break;
		}
	}

	Ok(())
}
