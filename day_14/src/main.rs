const INPUT: usize = 360_781;
const INPUT_SEQ: [usize; 6] = [3, 6, 0, 7, 8, 1];

fn loop_until<F: FnMut(&Vec<usize>, usize, usize) -> bool>(mut f: F) -> (Vec<usize>, usize, usize) {
	let mut board = Vec::with_capacity(INPUT + 10);
	board.push(3);
	board.push(7);
	let mut elf1_idx = 0;
	let mut elf2_idx = 1;
	loop {
		//and new recipes
		board.extend(
			(board[elf1_idx] + board[elf2_idx])
				.to_string()
				.chars()
				.map(|d| d.to_digit(10).unwrap() as usize),
		);
		// move elves
		elf1_idx += board[elf1_idx] + 1;
		elf1_idx %= board.len();
		elf2_idx += board[elf2_idx] + 1;
		elf2_idx %= board.len();
		if f(&board, elf1_idx, elf2_idx) {
			break;
		}
	}
	(board, elf1_idx, elf2_idx)
}

fn main() {
	//part 1
	let (board, _, _) = loop_until(|b, _, _| b.len() >= INPUT + 10);
	let (_, ten_last) = board.split_at(INPUT);
	let (ten_last, _) = ten_last.split_at(10);
	println!("{:?}", ten_last);

	//part2
	let mut found_offset = 0;
	let (board, _, _) = loop_until(|b, _, _| {
		let mut found = {
			if b.len() >= INPUT_SEQ.len() {
				let (_, last) = b.split_at(b.len() - INPUT_SEQ.len());
				last == INPUT_SEQ
			} else {
				false
			}
		};
		if !found && b.len() > INPUT_SEQ.len() {
			let (_, last) = b.split_at(b.len() - INPUT_SEQ.len() - 1);
			let (last, _) = last.split_at(INPUT_SEQ.len());
			found_offset = 1;
			found = last == INPUT_SEQ;
		}
		found
	});

	let (first, _) = board.split_at(board.len() - INPUT_SEQ.len() - found_offset);
	println!("{:?}", first.len());
}
