use util;

fn same_letter_not_same_case(a: char, b: char, from: Option<char>) -> bool {
	let contains = if let Some(x) = from {
		x.to_ascii_lowercase() == a.to_ascii_lowercase()
	} else {
		true
	};
	contains && a.to_ascii_uppercase() == b.to_ascii_uppercase() && a != b
}

fn get_adj_indices(input: &[u8], pattern: Option<char>) -> Vec<usize> {
	let mut out_idxs = Vec::new();
	let mut x = input.iter().enumerate().peekable();
	while let Some((idx, &val)) = x.next() {
		let first_char = val as char;
		if let Some((_, &val)) = x.peek() {
			let second_char = val as char;
			if same_letter_not_same_case(first_char, second_char, pattern) {
				x.next();
				out_idxs.push(idx);
			}
		}
	}
	out_idxs
}

fn fully_react_polymer(input: &[u8]) -> Vec<u8> {
	let mut out = input.to_vec();
	loop {
		let idxs = get_adj_indices(&out, None);
		if idxs.is_empty() {
			break;
		}
		let mut idx_adj = 0;
		for idx in idxs {
			out.remove(idx - idx_adj);
			out.remove(idx - idx_adj);
			idx_adj += 2;
		}
	}
	out
}

fn main() -> Result<(), std::io::Error> {
	//single line input for this day
	let input = util::read_text_file("input.txt")?.remove(0).into_bytes();

	//part 1
	let reacted = fully_react_polymer(&input);
	println!("{}", reacted.len());

	//part 2
	let result = [
		'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
		't', 'u', 'v', 'w', 'x', 'y', 'z',
	]
	.iter()
	.map(|letter| {
		let filtered = input
			.iter()
			.filter(|x| char::from(**x).to_ascii_lowercase() != *letter)
			.cloned()
			.collect::<Vec<_>>();
		fully_react_polymer(&filtered).len()
	})
	.min()
	.unwrap();
	println!("{}", result);

	Ok(())
}
