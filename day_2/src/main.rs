use util;

fn find_letter_freq(input: &[String], frequencies: &[usize]) -> Vec<usize> {
	let mut ret = vec![0; frequencies.len()];
	if !frequencies.is_empty() {
		input.iter().for_each(|y| {
			//assuming all ASCII and all lower case
			let mut letter_freqs = [0_usize; 26];
			y.as_bytes().iter().for_each(|b| {
				let idx = b - 97;
				letter_freqs[idx as usize] += 1;
			});
			for (idx, freq) in frequencies.iter().enumerate() {
				if letter_freqs.iter().any(|&x| x == *freq) {
					ret[idx] += 1;
				}
			}
		});
	}
	ret
}

fn find_one_diff_pair(input: &[String]) -> String {
	let mut ret_str = String::new();
	'outer: for i in 0..input.len() {
		'inner: for j in i + 1..input.len() {
			//assuming all inputs same len
			let mut num_diffs = 0_usize;
			let mut found_idx = 0_usize;
			for (idx, (l, r)) in input[i]
				.as_bytes()
				.iter()
				.zip(input[j].as_bytes().iter())
				.enumerate()
			{
				if *l != *r {
					num_diffs += 1;
					if num_diffs > 1 {
						continue 'inner;
					}
					found_idx = idx;
				}
			}
			let mut tmp_vec = input[i].as_bytes().to_vec();
			tmp_vec.remove(found_idx);
			ret_str = String::from_utf8(tmp_vec).unwrap();
			break 'outer;
		}
	}
	ret_str
}

fn main() -> Result<(), std::io::Error> {
	let input = util::read_text_file("input.txt")?;

	let part_one = find_letter_freq(&input, &[2, 3]);
	println!("{:?}", part_one[0] * part_one[1]);

	let part_two = find_one_diff_pair(&input);
	println!("{}", part_two);

	Ok(())
}
