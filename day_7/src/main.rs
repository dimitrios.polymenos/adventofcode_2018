use std::collections::HashMap;
use util;

#[derive(Default, Debug, Clone, Ord, PartialOrd, PartialEq, Eq)]
struct Job {
	id: u8,
	parents: Vec<u8>,
	children: Vec<u8>,
}

#[derive(Default, Debug, Copy, Clone)]
struct Worker {
	current_working_id: Option<u8>,
	secs_left: usize,
}

impl Worker {
	fn yield_finished(&mut self) -> Option<u8> {
		if self.secs_left == 0 {
			self.current_working_id.take()
		} else {
			None
		}
	}

	fn try_starting_a_new_job(&mut self, id: u8) -> bool {
		if self.current_working_id.is_none() {
			self.secs_left = id as usize - 64 + EXTRA_STEP;
			self.current_working_id = Some(id);
			true
		} else {
			false
		}
	}

	fn tick(&mut self, seconds: usize) {
		//debug underflow panic helps, not expecting to happen
		self.secs_left -= seconds;
	}
}

struct Scheduler {
	jobs: Vec<Job>,
}

impl Scheduler {
	fn new(j: &[Job]) -> Self {
		let mut jb = j.to_vec();
		jb.sort();
		Scheduler { jobs: jb }
	}

	fn find_next_available_job(&self) -> Option<(usize, u8)> {
		self
			.jobs
			.iter()
			.enumerate()
			.filter(|(_, j)| j.parents.is_empty())
			.min()
			.map(|(idx, j)| (idx, j.id))
	}

	fn consume_next_available_job(&mut self) -> Option<u8> {
		if let Some((idx, id)) = self.find_next_available_job() {
			self.jobs.remove(idx);
			Some(id)
		} else {
			None
		}
	}

	fn signal_job_finished(&mut self, id: u8) {
		//remove from the parents lists on the rest
		for v in self.jobs.iter_mut() {
			if let Some(index) = v.parents.iter().position(|x| *x == id) {
				v.parents.remove(index);
			}
		}
	}
}

const EXTRA_STEP: usize = 60;
const NUM_WORKERS: usize = 5;

fn main() -> Result<(), std::io::Error> {
	let input = {
		let mut tmp: HashMap<_, Job> = HashMap::new();
		util::read_text_file("input.txt")?
			.iter()
			.map(|x| {
				let mut it = x.split(' ').filter(|x| x.len() == 1).take(2);
				(it.next().unwrap(), it.next().unwrap())
			})
			.for_each(|(parent, name)| {
				let job_name = name.as_bytes()[0];
				let ent = tmp.entry(job_name).or_default();
				ent.id = job_name;

				let parent_name = parent.as_bytes()[0];
				ent.parents.push(parent_name);

				let ent = tmp.entry(parent_name).or_default();
				ent.id = parent_name;
				ent.children.push(job_name);
			});
		tmp
	};
	//extract only the values
	let input = input.values().cloned().collect::<Vec<_>>();

	//part 1
	let mut sch = Scheduler::new(&input);
	while let Some(id) = sch.consume_next_available_job() {
		print!("{}", id as char);
		sch.signal_job_finished(id);
	}
	println!("");

	//part 2
	let mut sch = Scheduler::new(&input);
	let mut workers = [Worker::default(); NUM_WORKERS];
	let mut total_secs_worked = 0;

	loop {
		for w in &mut workers {
			if let Some(old_id) = w.yield_finished() {
				sch.signal_job_finished(old_id);
			}
		}
		for w in &mut workers {
			if w.current_working_id.is_none() {
				if let Some(new_id) = sch.consume_next_available_job() {
					w.try_starting_a_new_job(new_id);
				}
			}
		}

		//instead of ticking sec by sec
		//i can tick until the quikest to finish non-idling worker is ready
		if let Some(secs) = workers
			.iter()
			.filter(|w| w.current_working_id.is_some())
			.min_by(|x, y| x.secs_left.cmp(&y.secs_left))
			.map(|x| x.secs_left)
		{
			total_secs_worked += secs;
			workers
				.iter_mut()
				.filter(|w| w.current_working_id.is_some())
				.for_each(|x| x.tick(secs));
		} else {
			break;
		}
	}
	println!("{}", total_secs_worked);
	Ok(())
}
