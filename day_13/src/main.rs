use std::collections::HashSet;
use util::Point;

#[derive(Debug)]
struct Board {
	data: Vec<Vec<TrackType>>,
}

impl Board {
	fn at_point(&self, p: Point) -> TrackType {
		self.data[p.x as usize][p.y as usize]
	}
}

#[derive(Clone, Debug)]
struct Train {
	pos: Point,
	dir: Direction,
	next_intersection_dir: Direction,
	collided: bool,
}

impl Train {
	fn new(x: i32, y: i32, d: Direction) -> Self {
		Train {
			pos: Point { x: x, y: y },
			dir: d,
			next_intersection_dir: Direction::Left,
			collided: false,
		}
	}

	fn advance(&mut self, board: &Board) {
		let current = board.at_point(self.pos);
		self.adjust_direction(current);
		let offset = self.dir.as_point();
		self.pos.x += offset.x;
		self.pos.y += offset.y;
	}

	fn adjust_direction(&mut self, current: TrackType) {
		match current {
			TrackType::Empty => panic!("Cannot currently be on empty"),
			TrackType::CurveBottomRight => match self.dir {
				Direction::Down => self.rotate_right(),
				Direction::Right => self.rotate_left(),
				Direction::Left => self.rotate_left(),
				Direction::Up => self.rotate_right(),
			},
			TrackType::CurveBottomLeft => match self.dir {
				Direction::Up => self.rotate_left(),
				Direction::Right => self.rotate_right(),
				Direction::Left => self.rotate_right(),
				Direction::Down => self.rotate_left(),
			},
			TrackType::Intersection => match self.next_intersection_dir {
				Direction::Up => self.next_intersection_dir = Direction::Right,
				Direction::Right => {
					self.rotate_right();
					self.next_intersection_dir = Direction::Left;
				}
				Direction::Left => {
					self.rotate_left();
					self.next_intersection_dir = Direction::Up;
				}
				_ => (),
			},
			_ => (),
			//TrackType::Vertical => assert!(self.dir == Direction::Up || self.dir == Direction::Down),
			//TrackType::Horizonal => assert!(self.dir == Direction::Left || self.dir == Direction::Right),
		}
	}

	fn rotate_left(&mut self) {
		self.dir = match self.dir {
			Direction::Up => Direction::Left,
			Direction::Down => Direction::Right,
			Direction::Left => Direction::Down,
			Direction::Right => Direction::Up,
		};
	}

	fn rotate_right(&mut self) {
		self.dir = match self.dir {
			Direction::Up => Direction::Right,
			Direction::Down => Direction::Left,
			Direction::Left => Direction::Up,
			Direction::Right => Direction::Down,
		};
	}
}

#[derive(Copy, Clone, Debug)]
enum TrackType {
	Empty,
	Vertical,
	Horizonal,
	CurveBottomRight,
	CurveBottomLeft,
	Intersection,
}

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
enum Direction {
	Left,
	Up,
	Right,
	Down,
}

impl Direction {
	fn as_point(self) -> Point {
		match self {
			Direction::Up => Point { x: 0, y: -1 },
			Direction::Down => Point { x: 0, y: 1 },
			Direction::Left => Point { x: -1, y: 0 },
			Direction::Right => Point { x: 1, y: 0 },
		}
	}
}

fn main() -> Result<(), std::io::Error> {
	let input = util::read_text_file("input.txt")?;
	let mut b = Board {
		data: vec![vec![TrackType::Empty; input.len()]; input[0].len()],
	};
	let mut trains: Vec<Train> = Vec::new();

	for (line_idx, line) in input.iter().enumerate() {
		for (byte_idx, c) in line.as_bytes().iter().enumerate() {
			b.data[byte_idx][line_idx] = match *c as char {
				' ' => TrackType::Empty,
				'-' => TrackType::Horizonal,
				'|' => TrackType::Vertical,
				'/' => TrackType::CurveBottomRight,
				'\\' => TrackType::CurveBottomLeft,
				'+' => TrackType::Intersection,
				'^' => {
					trains.push(Train::new(byte_idx as i32, line_idx as i32, Direction::Up));
					TrackType::Vertical
				}
				'v' => {
					trains.push(Train::new(
						byte_idx as i32,
						line_idx as i32,
						Direction::Down,
					));
					TrackType::Vertical
				}
				'>' => {
					trains.push(Train::new(
						byte_idx as i32,
						line_idx as i32,
						Direction::Right,
					));
					TrackType::Horizonal
				}
				'<' => {
					trains.push(Train::new(
						byte_idx as i32,
						line_idx as i32,
						Direction::Left,
					));
					TrackType::Horizonal
				}
				_ => panic!("Unexpected board parse token"),
			}
		}
	}

	//part 1
	let mut trains_part_1 = trains.clone();
	let mut dupes = HashSet::with_capacity(trains_part_1.len());
	for i in &trains_part_1 {
		dupes.insert(i.pos);
	}
	let crash_pos;
	'outer: loop {
		trains_part_1.sort_unstable_by_key(|k| (k.pos.y, k.pos.x));
		for i in &mut trains_part_1 {
			dupes.remove(&i.pos);
			i.advance(&b);
			if dupes.contains(&i.pos) {
				crash_pos = i.pos;
				break 'outer;
			} else {
				dupes.insert(i.pos);
			}
		}
	}
	println!("{:?}", crash_pos);

	//part 2
	let mut dupes = HashSet::with_capacity(trains_part_1.len());
	for i in &trains {
		dupes.insert(i.pos);
	}
	loop {
		trains.sort_unstable_by_key(|k| (k.pos.y, k.pos.x));
		let mut i = 0;
		loop {
			if i == trains.len() {
				break;
			}
			if trains.len() == 1 {
				break;
			}
			dupes.remove(&trains[i].pos);
			if trains[i].collided {
				i += 1;
				continue;
			}
			trains[i].advance(&b);
			let new_pos = trains[i].pos;
			if dupes.contains(&new_pos) {
				dupes.remove(&new_pos);
				trains.iter_mut().for_each(|x| {
					if x.pos == new_pos {
						x.collided = true;
					}
				});
			} else {
				dupes.insert(new_pos);
			}
			i += 1;
		}
		trains.retain(|x| !x.collided);
		if trains.len() == 1 {
			break;
		}
	}
	println!("{:?}", trains[0].pos);

	Ok(())
}
