use std::collections::HashMap;
use util::{Bounds, Point};

//input
const GRID_SERIAL: i32 = 7347;

fn calc_power_lvl(p: Point, serial: i32) -> i32 {
	let rack_id = p.x + 10;
	let m = (rack_id * p.y + serial) * rack_id;
	let hundreds = (m / 100) % 10;
	hundreds - 5
}

fn calc_grid_total_rec(
	top_l: Point,
	grid_w: u32,
	serial: i32,
	memo: &mut HashMap<(Point, u32), i32>,
) -> (i32, Point, u32) {
	let gw_i32 = grid_w as i32 - 1;
	let topxpl = top_l.x + gw_i32;
	let topypl = top_l.y + gw_i32;
	if memo.contains_key(&(top_l, grid_w)) {
		return (memo[&(top_l, grid_w)], top_l, grid_w);
	}
	let ret = {
		if grid_w == 1 {
			let bound = Bounds {
				top_l: top_l,
				bot_r: Point {
					x: topxpl,
					y: topypl,
				},
			};
			bound.iter().map(|x| calc_power_lvl(x, serial)).sum()
		} else {
			let b1 = Bounds {
				top_l: Point {
					x: topxpl,
					y: top_l.y,
				},
				bot_r: Point {
					x: topxpl,
					y: topypl,
				},
			};
			let b2 = Bounds {
				top_l: Point {
					x: top_l.x,
					y: topypl,
				},
				bot_r: Point {
					x: topxpl - 1,
					y: topypl,
				},
			};
			b1.iter().map(|x| calc_power_lvl(x, serial)).sum::<i32>()
				+ b2.iter().map(|x| calc_power_lvl(x, serial)).sum::<i32>()
				+ calc_grid_total_rec(top_l, grid_w - 1, serial, memo).0
		}
	};
	memo.insert((top_l, grid_w), ret);
	(ret, top_l, grid_w)
}

fn main() {
	let bound = Bounds {
		top_l: Point { x: 0, y: 0 },
		bot_r: Point { x: 297, y: 297 },
	};

	let mut memo = HashMap::with_capacity(300 * 300);
	//part 1
	let m = bound
		.iter()
		.map(|x| calc_grid_total_rec(x, 3, GRID_SERIAL, &mut memo))
		.max_by(|t1, t2| t1.0.cmp(&t2.0))
		.unwrap();
	println!("{:?}", m);

	//part 2
	//only takes about a few seconds in release mode
	//with memo enabled
	//after watching the solutions reddit thread, I should have used a
	//https://en.wikipedia.org/wiki/Summed-area_table
	let m = bound
		.iter()
		.map(|x| {
			(1..std::cmp::min(x.x, x.y))
				.map(|w| calc_grid_total_rec(x, w as u32, GRID_SERIAL, &mut memo))
				.max_by(|t1, t2| t1.0.cmp(&t2.0))
				.unwrap_or((0, Default::default(), 0))
		})
		.max_by(|t1, t2| t1.0.cmp(&t2.0))
		.unwrap();
	println!("{:?}", m);
}
