use std::collections::hash_map::Entry;
use std::collections::HashMap;
use util;

fn bools_to_num<T>(it: T) -> u32
where
	T: std::iter::Iterator,
	u32: std::convert::From<<T as std::iter::Iterator>::Item>,
{
	let mut ret = 0_u32;
	for (idx, v) in it.enumerate() {
		ret |= u32::from(v) << idx;
	}
	ret
}

fn take_step(
	transitions_map: &HashMap<u32, bool>,
	state: &mut Vec<bool>,
	idx_zero_offset: &mut i32,
) {
	state.extend(&[false, false, false]);
	state.insert(0, false);
	state.insert(0, false);
	state.insert(0, false);
	*idx_zero_offset += 3;
	let next_state = state
		.windows(5)
		.map(|x| transitions_map[&bools_to_num(x.iter().cloned())])
		.collect::<Vec<_>>();
	for (idx, s) in next_state.iter().enumerate() {
		state[2 + idx] = *s;
	}
	//trim front
	loop {
		let first = state.remove(0);
		if first {
			state.insert(0, first);
			break;
		}
		*idx_zero_offset -= 1;
	}
	//trim back
	loop {
		let last = state.pop().unwrap();
		if last {
			state.push(last);
			break;
		}
	}
}

fn count_state(state: &[bool], idx_zero_offset: i32) -> i32 {
	let mut res = 0;
	for (idx, b) in state.iter().enumerate() {
		if *b {
			res += idx as i32 - idx_zero_offset;
		}
	}
	res
}

fn main() -> Result<(), std::io::Error> {
	let input = util::read_text_file("input.txt")?;
	let initial_state = input[0]
		.split(' ')
		.nth(2)
		.unwrap()
		.as_bytes()
		.iter()
		.map(|x| *x == 35)
		.collect::<Vec<_>>();

	let transitions_map = input[2..]
		.iter()
		.map(|x| {
			let mut it = x.split(" => ");
			let k = bools_to_num(it.next().unwrap().as_bytes().iter().map(|x| *x == 35));
			let v = it.next().unwrap() == "#";
			(k, v)
		})
		.collect::<HashMap<_, _>>();

	//part 1
	let mut start = initial_state.clone();
	let mut idx_zero_offset = 0;
	for _ in 0..20 {
		take_step(&transitions_map, &mut start, &mut idx_zero_offset);
	}
	let res = count_state(&start, idx_zero_offset);
	println!("{}", res);

	//part 2
	let mut start = initial_state.clone();
	let mut idx_zero_offset = 0;
	//loop until we get the same bit pattern twice
	let mut iters: HashMap<i32, (i32, usize)> = HashMap::new();
	let mut i = 0_usize;
	let mut res_prev = count_state(&start, idx_zero_offset);
	let mut res_prev_z = count_state(&start, 0);
	while let Entry::Vacant(e) = iters.entry(res_prev_z) {
		e.insert((res_prev, i));
		take_step(&transitions_map, &mut start, &mut idx_zero_offset);
		res_prev = count_state(&start, idx_zero_offset);
		res_prev_z = count_state(&start, 0);
		i += 1;
	}
	let iters_left = 50_000_000_000 - iters[&res_prev_z].1;
	let score_diff = res_prev - iters[&res_prev_z].0;

	//this being one simplifies things a lot
	let _idx_diff = i - iters[&res_prev_z].1;
	//otherwise I would have to (score_diff * iters_left / _idx_diff)
	//and start looping again for (iters_left % _idx_diff) times to reach 50bil
	//tricky bit is how to derive the state to begin looping

	let count = iters_left * score_diff as usize + iters[&res_prev_z].0 as usize;
	println!("{}", count);
	Ok(())
}
