use std::collections::HashMap;
use util;

fn part_one(parsed_input: &[i32]) -> i32 {
	parsed_input.iter().sum::<i32>()
}

fn part_two(parsed_input: &[i32]) -> i32 {
	let mut sum_freqs = HashMap::new();
	let mut sum = 0;
	sum_freqs.entry(0).or_insert(1);
	for i in parsed_input.iter().cycle() {
		sum += i;
		let kv = sum_freqs.entry(sum).or_insert(0);
		*kv += 1;
		if *kv > 1 {
			return sum;
		}
	}
	unreachable!()
}

fn main() -> Result<(), std::io::Error> {
	let s: Vec<i32> = util::read_text_file("input.txt")?
		.iter()
		.map(|x| x.parse::<i32>().unwrap())
		.collect();

	println!("{}", part_one(&s));
	println!("{}", part_two(&s));
	Ok(())
}
